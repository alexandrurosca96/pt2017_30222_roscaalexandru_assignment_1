import org.junit.Assert;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Map;
import java.util.TreeMap;

/**
 * Created by Rosca on 07.03.2017.
 */
public class CalculatorTest {
    String coef1 = "2X^2 + X + 2";
    String coef2 = "X^5 + 3X^1 + 1";
    Polynom p1 = new Polynom(coef1);
    Polynom p2 = new Polynom(coef2);
    private Calculator calc = new Calculator();
    //TODO: test pentru convertString
    @Test
    public void converStringTest() throws Exception{
        String expected = "2X^2+1X^1+2";

        Assert.assertEquals(expected, p1.toString());
    }

    @Test
    public void multiplicationTest() throws Exception {

         String coef3 = "2+7X^1+5X^2+6X^3+2X^5+1X^6+2X^7";

        Polynom expected = new Polynom(coef3);
        Polynom rez = calc.multiplication(p1, p2);

        Assert.assertTrue(expected.equals(rez));
    }

    @Test
    public void additionTest() throws Exception {

        String coef3 = "X^5+4X+2X^2+3";

        Polynom expected = new Polynom(coef3);
        Polynom rez = calc.addition(p1, p2);


        Assert.assertTrue(expected.equals(rez));
    }
    @Test
    public void subtractionTest() throws Exception {
        String coef3 = "-X^5 + 2x^2 - 2X + 1";
        Polynom p1 = new Polynom(coef1);
        Polynom p2 = new Polynom(coef2);

        Polynom expected = new Polynom(coef3);
        Polynom rez = calc.subtraction(p1, p2);

        Assert.assertTrue(expected.equals(rez));
    }

    @Test
    public void divideTest() throws  Exception{
        ArrayList<Polynom> result = new ArrayList<>();
        String coef2 = "X^2 + X + 2";
        String coef1 = "X^5 + 3X + 1";
        String coefCat = "X^3-X^2-X+3";
        String coefRest = "2X - 5";
        Polynom p1 = new Polynom(coef1);
        Polynom p2 = new Polynom(coef2);
        result = calc.divide(p1, p2);

        Assert.assertTrue((new Polynom(coefCat)).equals(result.get(0)));
        Assert.assertTrue((new Polynom(coefRest)).equals(result.get(1)));

    }

    @Test
    public void derivateTest() throws Exception{
        String coef3 = "5X^4 + 3";
        Assert.assertTrue((new Polynom(coef3)).equals(calc.derivate(p2)));
    }
    @Test
    public void integrateTest() throws Exception{
        Polynom p2 = new Polynom("2X^3 + X + 1");
        String coef3 = "0.5X^4 + 0.5X^2 + 1X";
        Assert.assertTrue((new Polynom(coef3)).equals(calc.integrate(p2)));
    }

}
