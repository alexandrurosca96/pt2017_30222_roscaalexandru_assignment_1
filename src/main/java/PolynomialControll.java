import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

/**
 * Created by Rosca on 11.03.2017.
 */
public class PolynomialControll {
    private PolynomialView view;
    private Calculator calc;

    PolynomialControll(PolynomialView view, Calculator calc){
        this.view = view;
        this.calc = calc;

        view.addActionSum( new SumList());
        view.addActionSub(new SubList());
        view.addActionMul(new MulList());
        view.addActionDer(new DerList());
        view.addActionDiv(new DivList());
        view.addActionInt(new IntList());
    }

    class IntList implements ActionListener{
        public void actionPerformed(ActionEvent e){
            Polynom p1 = new Polynom();
            String rez;
            try{
                p1.convertString(view.getPol1());
                rez = calc.integrate(p1).toString();
                if(rez.equals("")){
                    rez = "0";
                }
                view.setTextArea("P1: " + p1.toString()+ "    \n" +  "\nInt= " + calc.integrate(p1).toString());
            }
            catch (Exception er){
                view.setTextArea("Invalid polynomlias \nTry again!");
            }
        }
    }


    class DerList implements ActionListener{
        public void actionPerformed(ActionEvent e){
            Polynom p1 = new Polynom();
            String rez;
            try{
                p1.convertString(view.getPol1());
                rez = calc.derivate(p1).toString();
                if(rez.equals("")){
                    rez = "0";
                }
                view.setTextArea("P1: " + p1.toString()+ "    \n" +  "\nInt= " + rez);
            }
            catch (Exception er){
                view.setTextArea("Invalid polynomlias \nTry again!");
            }
        }
    }


    class SubList implements ActionListener{
        public void actionPerformed(ActionEvent e){
            Polynom p1 = new Polynom();
            Polynom p2 = new Polynom();
            String rez;
            try{
                p1.convertString(view.getPol1());
                p2.convertString(view.getPol2());
                rez = calc.subtraction(p1,p2).toString();
                if(rez.equals("")){
                    rez = "0";
                }
                view.setTextArea("P1: " + p1.toString()+ "   - \n" + "P2: " + p2.toString() + "\nRes= " + rez);
            }
            catch (Exception er){
                view.setTextArea("Invalid polynomlias \nTry again!");
            }
        }
    }

    class DivList implements ActionListener{
        public void actionPerformed(ActionEvent e){
            Polynom p1 = new Polynom();
            Polynom p2 = new Polynom();
            ArrayList<Polynom> res = new ArrayList<>();
            try{
                p1.convertString(view.getPol1());
                p2.convertString(view.getPol2());
                if(p1.getFirstKey() < p2.getFirstKey()){
                    view.setTextArea("Reverse polynomials!");
                }
                else {
                    res = calc.divide(p1, p2);
                    view.setTextArea("P1: " + p1.toString() + "   \\ \n" + "P2: " + p2.toString() + "\nCat= " + res.get(0).toString() + "\nRest= " + res.get(1).toString());
                }
            }

            catch (Exception er){
                view.setTextArea("Invalid polynomlias \nTry again!");
            }
        }

    }



    class MulList implements ActionListener{
        public void actionPerformed(ActionEvent e){
            String rez;
            Polynom p1 = new Polynom();
            Polynom p2 = new Polynom();
            try{
                p1.convertString(view.getPol1());
                p2.convertString(view.getPol2());
                rez = calc.multiplication(p1,p2).toString();
                if(rez.equals("")){
                    rez = "0";
                }
                view.setTextArea("P1: " + p1.toString()+ "   * \n" + "P2: " + p2.toString() + "\nRes= " + rez);
            }
            catch (Exception er){
                view.setTextArea("Invalid polynomlias \nTry again!");
            }
        }
    }



    class SumList implements ActionListener{
        public void actionPerformed(ActionEvent e){
            String rez;
            Polynom p1 = new Polynom();
            Polynom p2 = new Polynom();
            try{
                p1.convertString(view.getPol1());
                p2.convertString(view.getPol2());
                rez = calc.addition(p1,p2).toString();
                if(rez.equals("")){
                    rez = "0";
                }
                view.setTextArea("P1: " + p1.toString()+ "   + \n" + "P2: " + p2.toString() + "\nRes= " + rez);
            }
            catch (Exception er){
                view.setTextArea("Invalid polynomlias \nTry again!");
            }
        }
    }
}

