import java.util.Collection;
import java.util.Collections;
import java.util.TreeMap;
import java.util.Map;

/**
 * Created by Rosca on 06.03.2017.
 */
public class Polynom {

    private TreeMap<Integer, Double> monom = new TreeMap<Integer, Double>(Collections.reverseOrder());  //monom<grad, coeficient>

    Polynom(String input){
        convertString(input);
    }

    Polynom (TreeMap<Integer, Double> m){
        monom.putAll(m);
    }
    Polynom (){};

    Polynom (int key, double value){
        monom.put(key, value);

    }

    public void addKeyValue(int key, double value){
        if(monom.containsKey(key)){
            monom.put(key, value + monom.get(key));
        }
        else{
            monom.put(key, value);
        }
    }
    public TreeMap<Integer,Double> getMonom(){
        return monom;
    }


    /**
     * Aceasta functie produce o reprezentare sub forma unui string al Polinomului
     * @return
     */
    public String toString() {
        String format = "%.2f";
        String pol = "";

        for(Map.Entry<Integer, Double> entry : monom.entrySet()){
            if (entry.getValue().intValue() == entry.getValue()){   //daca coeficientii sunt intregi sa se afiseze fara zecimale
                format = "%.0f";
            }

            if(entry.getKey() == 0){//daca avem gradul 0
                if(entry.getValue() > 0) {
                    pol = pol + "+" + String.format(format,entry.getValue());
                }
                else{
                    pol = pol + String.format(format,entry.getValue());
                }
            continue;
            }

            if(entry.getValue() > 0){
                if(pol.equals("")) {
                    pol = pol + String.format(format,entry.getValue()) + "X^" + entry.getKey();
                }
                else{
                    pol = pol + "+" + String.format(format,entry.getValue()) + "X^" + entry.getKey();
                }
                continue;
            }
            if(entry.getValue() < 0){
                pol = pol  + String.format(format,entry.getValue()) + "X^" + entry.getKey();
            }
        }

        return pol;
    }

    public int getFirstKey(){
        return monom.firstKey();
    }
    public double getValueFromKey(int key){
        return monom.get(key);
    }


    /**
     * Aceasta functie converteste stringul primit ca si parametru in monoame (coeficienti si grade)
     * @param input - Strigul care trebuie interpretat
     * Primul pas este sa se desparta stringul in dupa '+' si '-' si se obtine un array de stringuri "parts"
     * Apoi se ia array-ul de stringuri si se prelucreaza dupa cazurile particulare care pot aparea
     * Pot aparea urmatoarele cazuri:
     * - "2X^3" se desparte dupa X^ si se obtine '2' si '3' care reprezinta coeficientul si gradul
     * - "X^3" se desparte dupa X^ si se obtine 'null' si '3' => coef = 1; grad = 3
     * - "-X^3" se desparte dupa X^ si obtine '-' si '3' => coef = -1; grad = 3;
     * - "2X" se desparte dupa X si obtine '2' si 'null' =>coef = 2; grad = 1;
     * - "X" => coef = 1; grad = 1
     * - "-X" => coef = -1; grad =1
     */
    public void convertString(String input){
        input = input.replaceAll("\\s","");
        input = input.toUpperCase();
        String [] parts ;
        String [] aux ;


      // System.out.println(input);
        parts = input.split("(?=[+-])");//split by - or + and keep it
        for(String part : parts){
            if(part.contains("X^")){
                aux = part.split("[X,x]\\^");
                try {
                    addKeyValue(Integer.parseInt(aux[1]), Double.parseDouble(aux[0]));
                }
                catch(Exception e){
                    if(aux[0].equals("-")){
                        addKeyValue(Integer.parseInt(aux[1]), new Double(-1));
                    }
                    else {
                        addKeyValue(Integer.parseInt(aux[1]), new Double(1));
                    }
                }
                continue;
            }
            if(part.contains("X")){
                    if(part.equals("X")){addKeyValue(1,1); continue;};
                    aux = part.split("[X,x]");
                    try{
                        addKeyValue(new Integer("1"), Double.parseDouble(aux[0]));
                    }catch(Exception e){
                        if(aux[0].equals("-"))
                             addKeyValue(new Integer("1"), new Double(-1));// -X
                        else
                             addKeyValue(new Integer("1"), new Double(1));// X
                    }

                continue;
            }
            addKeyValue(new Integer("0"), Double.parseDouble(part));    //monoame cu grad 0

        }
    }

    @Override
    public boolean equals(Object pol) {
        if(monom.firstKey() != ((Polynom)pol).getFirstKey()){//ma asigur ca ambele au acceasi dimensiune
            return false;
        }
        for(Map.Entry<Integer, Double> entry: ((Polynom)pol).getMonom().entrySet()){
            if(entry.getValue().doubleValue() != monom.get(entry.getKey()).doubleValue()){
                return false;
            }
        }
        return true;

    }



}
