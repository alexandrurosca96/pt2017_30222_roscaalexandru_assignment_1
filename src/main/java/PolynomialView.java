import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionListener;

/**
 * Created by Rosca on 11.03.2017.
 */
public class PolynomialView {
    JFrame frame = new JFrame();
    JPanel panelPol = new JPanel();
    JPanel panelEc = new JPanel();
    JLabel labelPol1 =  new JLabel("P1:");
    JLabel labelPol2 =  new JLabel("P2:");
    JTextField textFieldPol1 = new JTextField();
    JTextField textFieldPol2 = new JTextField();
    JButton buttonAdd = new JButton("+");
    JButton buttonSub = new JButton("-");
    JButton buttonMul = new JButton("*");
    JButton buttonDiv = new JButton("/");
    JButton buttonDer = new JButton("d/dx");
    JButton buttonInt = new JButton("integ");
    JTextArea textArea = new JTextArea(50,10);


    PolynomialView() {
        frame.setSize(400, 400);
        frame.getContentPane().setLayout(null);
        panelPol.setLayout(null);
        textArea.setEditable(false);

        panelEc.setLayout(null);
        //position
        panelEc.setBounds(30, 30, 350, 100);
        panelPol.setBounds(80, 130, 300, 250);

        labelPol1.setBounds(20, 20, 40, 20);
        labelPol2.setBounds(180, 20, 40, 20);

        buttonAdd.setBounds(20, 20, 70, 40);
        buttonSub.setBounds(100, 20, 70, 40);
        buttonMul.setBounds(180, 20, 70, 40);
        buttonDiv.setBounds(20, 80, 70, 40);
        buttonInt.setBounds(100, 80, 70, 40);
        buttonDer.setBounds(180, 80, 70, 40);
        textArea.setBounds(0, 140, 270, 100);

        textFieldPol1.setBounds(40, 20, 130, 20);
        textFieldPol2.setBounds(200, 20, 130, 20);

        //adding elements
        panelEc.add(labelPol1);
        panelEc.add(labelPol2);
        panelEc.add(textFieldPol1);
        panelEc.add(textFieldPol2);

        panelPol.add(buttonAdd);
        panelPol.add(buttonDer);
        panelPol.add(buttonDiv);
        panelPol.add(buttonInt);
        panelPol.add(buttonMul);
        panelPol.add(buttonSub);
        panelPol.add(textArea);

        //visibility
        panelEc.setVisible(true);
        panelPol.setVisible(true);
        panelPol.setVisible(true);
        frame.add(panelEc);
        frame.add(panelPol);
        frame.setVisible(true);
    }

    public void addActionSum(ActionListener listener){
        buttonAdd.addActionListener(listener);
    }
    public void addActionSub(ActionListener listener){
        buttonSub.addActionListener(listener);
    }

    public void addActionDer(ActionListener listenerSum){
        buttonDer.addActionListener(listenerSum);
    }

    public void addActionInt(ActionListener listenerSum){
        buttonInt.addActionListener(listenerSum);
    }

    public void addActionMul(ActionListener listenerSum){
        buttonMul.addActionListener(listenerSum);
    }

    public void addActionDiv(ActionListener listenerSum){
        buttonDiv.addActionListener(listenerSum);
    }

    public String getPol1 (){
        return textFieldPol1.getText();
    }
    public String getPol2(){
        return textFieldPol2.getText();
    }
    public void setTextArea(String string){
        textArea.setText(string);
    }

}
