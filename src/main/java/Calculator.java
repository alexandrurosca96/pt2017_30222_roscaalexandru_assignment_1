import java.util.*;

/**
 * Created by Rosca on 06.03.2017.
 */
public class Calculator {


    /**
     *
     * @param p1    - first Polynomial
     * @param p2    - second Polynomial
     * @return      - p1 + p2
     * Pentru a calcula suma polinoamelor se declara o variabila de tip TreeMap (rez) in care sa fie pastrat rezultatul,
     * aceasta a fost initializata cu valorile din p1, s-a parcurs al doilea polinom si daca se gasea cheia in "rez", valoarea
     * de la respectiva cheie era updatata, iar daca nu se gasea, se introducea
     */
    public Polynom addition(Polynom p1, Polynom p2){
        TreeMap<Integer, Double> rez = new TreeMap<Integer, Double>(p1.getMonom());

        for(Map.Entry<Integer, Double> entry : p2.getMonom().entrySet()){
            if(rez.containsKey(entry.getKey())){
                rez.replace(entry.getKey(), rez.get(entry.getKey()) + entry.getValue());
            }
            else{
                rez.put(entry.getKey(), entry.getValue());
            }
        }
        deleteZero(rez);
        return new Polynom(rez);
    }

    /**
     *
     * @param p1    -first pol
     * @param p2    -second pol
     * @return      - p1 -p2
     * Pentru a calcula p1-p2, se inmulteste al doilea polinom cu "-1" si apoi se aduna cele doua
     */
    public Polynom subtraction(Polynom p1, Polynom p2){
        Polynom rez = new Polynom();
        rez.addKeyValue(0, -1);//pol = -1
        rez = multiplication(p2, rez);
        return addition(p1,rez);
    }

    /**
     *
     * @param p     - pol
     * @return      - derivata polinomului
     * S-a declarat o variabila de tip TreeMap("rez") in care se pastreaza rezultatul, se parcuge polinomul, se afla
     * coeficientii si gradele monoamelor si se introduc in "rez" cu valori updatate, Key: grad -1 ; Value: coef*grad;
     * caz special ar fi cand gradul este 0 si trece la urmatoarea iteratie
     */
    public Polynom derivate (Polynom p){
        TreeMap<Integer, Double> rez = new TreeMap<Integer, Double>();
        if((int)p.getFirstKey() == 0){
            return new Polynom(rez);
        }

        for(Map.Entry<Integer, Double> entry : p.getMonom().entrySet()){
            int exponent = entry.getKey();
            double coef = entry.getValue();
            if(exponent == 0) continue;

            rez.put(exponent - 1, coef * exponent);

        }

    return new Polynom(rez);
    }

    /**
     *
     * @param pol     - pol
     * @return      - derivata polinomului
     * S-a declarat o variabila de tip TreeMap("rez") in care se pastreaza rezultatul, se parcuge polinomul, se afla
     * coeficientii si gradele monoamelor si se introduc in "rez" cu valori updatate, Key: grad + 1; Value: coef/(grad-1)
     */
    public Polynom integrate(Polynom pol){
        TreeMap<Integer, Double> rez = new TreeMap<Integer, Double>();
        for(Map.Entry<Integer, Double> entry : pol.getMonom().entrySet()){
            rez.put(entry.getKey() + 1, (entry.getValue() / (entry.getKey() + 1)));
        }
        return new Polynom(rez);
    }

    /**
     *
     * @param p1    -first pol
     * @param p2    -secodn pol
     * @return      - p1*p2
     * Se declara un polinom "pol" in care se salveaza rezultatul final; se parcurg cele doua polinoame si pentru fiecare
     * combinatie de monoame din cele doua polioane  se calculeaza noua valaore pentru monomul rezultat si se aduna la "pol"
     */
    public Polynom multiplication (Polynom p1, Polynom p2){
       Polynom pol = new Polynom();
        for(Map.Entry<Integer, Double> entry1: p1.getMonom().entrySet()){
            for(Map.Entry<Integer, Double> entry2: p2.getMonom().entrySet()){
                int exponent = entry1.getKey() + entry2.getKey();
                double coef = entry1.getValue() * entry2.getValue();

                pol = addition(new Polynom(exponent, coef), pol);
            }
        }
        deleteZero(pol.getMonom());
        return pol;
    }

    /**
     *
     * @param p1    - first pol
     * @param p2    - second pol
     * @return      - Array de polinoame care contin catul si restul impartirii polinomului p1 la p2
     * @throws Exception    - in caz ca al doilea polinom este 0 se arunca o exceptie
     * Initial s-a presupus ca Restul este egal cu P1 si Catul este egal cu 0;
     * "exp" - gradul polinomului P2; "coef" - coeficientul gradului maxim al lui P2
     * Catul se formeaza la fiecare iteratie ca fiind monomul de grad maxim al polinomului Rest impartit la monomul
     * de grad maxim al polinomului P2
     * Restul se formeaza la fiecare iteratie ca fiind Restul vechi minus produsul dintre Cat si P2
     * Acest proces se repeta cat timp Restul are gradul mai mare sau egal cu gradul polinomului P2
     * La fiecare iteratie se verifica daca restul nu are coeficienti 0 si daca are se sterg
     * Cazul particular ar fi cand Restul este 0 si atunci se introduce un monom cu coef 0 si gradul 0 si se iese din bucla
     */
    public ArrayList<Polynom> divide (Polynom p1, Polynom p2)throws Exception{
        //exponent and coef of the recond Polynomial
        int exp = p2.getFirstKey();
        double coef = p2.getValueFromKey(exp);

        Polynom pol, cat = new Polynom();
        Polynom rest = new Polynom(p1.getMonom());

        ArrayList<Polynom> result = new ArrayList<Polynom>();

        if(p2.getMonom().equals(new Polynom(0,0).getMonom())){
            throw new Exception("divide by 0");
        }
        else {
            while (rest.getFirstKey() >= p2.getFirstKey()) {

                int exp1 = rest.getFirstKey() - exp;
                double coef1 = rest.getMonom().get(rest.getFirstKey()) / coef;
                cat.addKeyValue(exp1, coef1);
                pol = multiplication(new Polynom(exp1, coef1), p2);
                rest = subtraction(rest, pol);

                deleteZero(rest.getMonom());
                if (rest.getMonom().isEmpty()) {
                    rest.addKeyValue(0, 0);
                    break;
                }

            }
            result.add(cat);
            result.add(rest);

            return result;
        }
    }

    /**
     *
     * @param map
     * Se sterg elemetele care au coeficientul 0
     */
    public void deleteZero(TreeMap<Integer, Double> map){
        map.entrySet().removeIf(e -> (e.getValue() == 0));
    }

}
